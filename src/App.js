import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import firebase from 'firebase';
import reducers from './reducers';
import LoginForm from './screens/LoginForm';

export default class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyBEIWFrZkc4lFmsYyzxJTaYPCeLCHmI5Ho',
            authDomain: 'manager-6f8bb.firebaseapp.com',
            databaseURL: 'https://manager-6f8bb.firebaseio.com',
            projectId: 'manager-6f8bb',
            storageBucket: 'manager-6f8bb.appspot.com',
            messagingSenderId: '273966670163'
        };
        firebase.initializeApp(config);
    }

    render() {
        return (
            <Provider store={createStore(reducers)}>
                <LoginForm />
            </Provider>
        );
    }
}
