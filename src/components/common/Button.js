import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, displayText, bgColor, displayColor, borColor }) => {
    const { buttonStyle, textStyle } = styles(bgColor, borColor, displayColor);

    return (
        <TouchableOpacity onPress={onPress} style={buttonStyle}>
            <Text style={textStyle}>{displayText}</Text>
        </TouchableOpacity>
    );
};

const styles = (bgColor, borColor, displayColor) => ({
    buttonStyle: {
        alignSelf: 'stretch',
        backgroundColor: bgColor,
        borderColor: borColor,
        borderWidth: 1,
        borderRadius: 10,
        flex: 1
    },
    textStyle: {
        alignSelf: 'center',
        fontSize: 17,
        fontWeight: '600',
        color: displayColor,
        padding: 10
    }
});

export { Button };
