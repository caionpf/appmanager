import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
    return (
        <View style={styles.containerStyle}>
            {props.children}
        </View>
    );
};

const styles = {
    containerStyle: {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#f7f7f7',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#c7c7c7',
        position: 'relative'
    }
};

export { CardSection };
