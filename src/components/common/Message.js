import React from 'react';
import { Text, View } from 'react-native';

const Message = (props) => {
    const { textStyle, viewStyle } = styles(props.messageType);

    return (
        <Text style={textStyle}>
            {props.children}
        </Text>
    );
};

styles = function(messageType) {

    var textColor, backgroundColor;
    var display = 'none'
    switch(messageType) {
        case 'warning':
            textColor = '#726500';
            backgroundColor = '#FFFA9B';
            break;
        case 'error':
            textColor = '#A30000';
            backgroundColor = '#FF9B9B';
            break;
        case 'info':
            textColor = '#004691';
            backgroundColor = '#84BFFF';
            break;
        case 'success':
            textColor = '#155100';
            backgroundColor = '#C6FFB2';
            break;
        default:
            textColor = null;
            backgroundColor = null;
    }

    if (messageType != '') display = 'flex';

    return {
        textStyle: {
            fontSize: 16,
            borderRadius: 4,
            marginTop: 7,
            marginBottom: 3,
            alignSelf: 'center',
            textAlign: 'center',
            width: '100%',
            color: textColor,
            backgroundColor: backgroundColor,
            display: display
        }
    }
};

export { Message };
