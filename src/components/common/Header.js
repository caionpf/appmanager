import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {
    const { textStyle, viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={[textStyle, { color: props.displayColor }]}>
                {props.displayText}
            </Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#E7E7E7',
        alignItems: 'center',
        justifyContent: 'center',
        //borderBottomWidth: 1,
        //borderBottomColor: '#C7C7C7',
        elevation: 9
        //height: 60
        //paddingTop: 15
    },
    textStyle: {
        fontSize: 35
        //color: '#474747',
        //fontFamily: 'monospace'
    }
};

export { Header };
