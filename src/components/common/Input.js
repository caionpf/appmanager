import React from 'react';
import { Text, View, TextInput } from 'react-native';

const Input = ({ displayText, value, onChangeText, placeholder, secureTextEntry }) => {  
    const { labelStyle, inputStyle, viewStyle } = styles;
    
    return (
        <View style={viewStyle} >
            <Text style={labelStyle} >{displayText}</Text>
            <TextInput 
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                value={value}
                onChangeText={onChangeText}
                style={inputStyle}                     
            />
        </View>
    );
};

const styles = {
    labelStyle: {
        paddingLeft: 20,
        fontSize: 18,
        flex: 1
    },
    inputStyle: {
        color: '#000000',
        fontSize: 18,
        paddingRight: 5,
        paddingLeft: 5,
        lineHeight: 23,
        flex: 2
    },
    viewStyle: {
        height: 50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export { Input };
