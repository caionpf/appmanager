import React from 'react';
import { Text, View } from 'react-native';

const Title = ({ displayText, displayColor }) => {
    const { textStyle, viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={[textStyle, { color: displayColor }]}>
                {displayText}
            </Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#F0F0F0',
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        width: null
    },
    textStyle: {
        fontSize: 22
    }
};

export { Title };
