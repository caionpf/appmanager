import React from 'react';
import { Text, View, ActivityIndicator } from 'react-native';

const FakeButton = ({ displayText }) => {
    const {viewStyle, textStyle, spinnerStyle} = styles;

    return (
        <View style={viewStyle}>
            <ActivityIndicator color="#000000" size="small" />
            <Text style={textStyle}>{displayText}</Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        alignSelf: 'stretch',
        backgroundColor: '#878787',
        borderColor: '#777777',
        borderWidth: 1,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 1
    },
    textStyle: {
        alignSelf: 'center',
        fontSize: 17,
        fontWeight: '600',
        color: '#e7e7e7',
        padding: 10
    }
};

export { FakeButton };