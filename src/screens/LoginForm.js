import React, { Component } from 'react';
import { Card, CardSection, Input, Button, Title } from '../components/common';

export default class LoginForm extends Component {
    render() {
        return (
            <Card>
                <CardSection>
                    <Title 
                        displayText="Sign in to manager"
                        displayColor="#111111"
                    />
                </CardSection>
                <CardSection>
                    <Input 
                        displayText="E-Mail"
                        placeholder="my.email@email.com"
                    />
                </CardSection>
                <CardSection>
                    <Input 
                        displayText="Password"
                        placeholder="********"
                    />
                </CardSection>
                <CardSection>
                    <Button 
                        displayText="Login"
                        backgroundColor="#77FF77"
                        borColor="#114411"
                        displayColor="#114411"
                    />
                </CardSection>
            </Card>
        );
    }
}
